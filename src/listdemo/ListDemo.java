/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listdemo;

import java.util.List;
import java.util.ArrayList;
import java.util.Vector;

public class ListDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Creamos una lista de palabras
        ArrayList<String> lista = new ArrayList<>();
        //Agregamos palabras
        lista.add("Hola");
        lista.add("mundo");
        lista.add("mundo");
        lista.add("mundo");
        lista.add("mundo");
        //Cuidado porque puedo agregar elementos null y si los intento mostrar tendré error:
        lista.add(null);
        //Mostramos el número de elementos que contiene:
        System.out.println("Tamaño: " + lista.size());
        
        String res = "";        
        
        //foreach para concatenar. Nos va a dar error cuando llegue a null porque no implementa toUpperCase
        for(String elemento : lista){
            //res += elemento.toUpperCase() + " ";
        }
        System.out.println(res);
        
        //Mostramos con un sólo sout
        System.out.println(lista);
        
        //Me devuelve la primera posición en la que encuentra la palabra
        int index = lista.indexOf("mundo");
        System.out.println("Primera aparición de mundo: " + index);
        //No existe por lo que me devuelve -1
        index = lista.indexOf("prueba");
        System.out.println("Primera aparición de prueba: " + index);
        
        //Sólo borra la primera aparición de la palabra mundo
        lista.remove("mundo");       
        System.out.println(lista);
        
        //Si quiero borrar todos puedo crear un método que borre todos
        removeAll(lista, "mundo");
        System.out.println(lista);
        
        java.util.Set<String> set = new java.util.HashSet<>(java.util.Arrays.asList("hola".split("")));
        System.out.println(set);
                
    }
    
    public static void removeAll(ArrayList<String> lista, String eliminar){        
        while(lista.contains(eliminar)){
            lista.remove(eliminar);
        }
    }
       
    
}

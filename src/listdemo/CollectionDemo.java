/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listdemo;

import java.util.Collection;
import java.util.Iterator;

public class CollectionDemo {
    
    public static void main(String[] args){
        Collection<String> collectionStrings = new java.util.ArrayList<>();
        collectionStrings.add("hola");
        collectionStrings.add("mundo");
        collectionStrings.add("test");
        System.out.println(collectionStrings.contains("hola"));
        
        StringBuilder aux = new StringBuilder("");
        //Recorrido de lectura
        for(String palabra : collectionStrings ){
            aux.append(palabra.toUpperCase()).append(" ");
        }
        System.out.println("Aux: " + aux);
        
        //Recorrido que modifica la lista
        Iterator<String> it = collectionStrings.iterator();
        //Borro las palabras que contengan un caracter 't'
        while (it.hasNext()) {
            String next = it.next();            
            if(next.contains("t")){
                it.remove();
            }
        }
        System.out.println("Tras eliminar las palabras que contengan t: " + collectionStrings);
        
        Collection<String> collection2 = new java.util.ArrayList<>();
        collection2.add("mundo");
        collection2.add("elemento");
        
        //Me quedo con los elementos que estén en ambas colecciones
        collectionStrings.retainAll(collection2);
        System.out.println("Intersección: " + collectionStrings);
        
        //Transformación a Array
        String[] array = new String[collectionStrings.size()];
        array = collectionStrings.toArray(array);
        
        System.out.println("Recorro array: ");
        for(String text : array){
            System.out.println(text);
        }
    }
}

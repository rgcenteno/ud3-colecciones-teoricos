/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listdemo;

import java.util.Set;

public class SetDemo {
    
    public static void main(String[] args){
        
        quitarRepetidosLista();
        
        ejemploCaracteres();
                
        
    }
    
    private static void quitarRepetidosLista(){
        java.util.List<String> nombres = new java.util.ArrayList<>();
        nombres.add("Pedro");
        nombres.add("Juan");
        nombres.add("Manuel");
        nombres.add("Jacinto");
        nombres.add("Pedro");
        nombres.add("Roberto");
        nombres.add("Juan");
        System.out.println("Nombres: " + nombres);
        Set<String> setNombres = new java.util.HashSet<>(nombres);
        System.out.println("Nombres sin repetición: " + setNombres);
    }
    
    private static void ejemploCaracteres(){
        //Ejemplos con caracteres
        Set<Character> set = new java.util.HashSet<>();
        set.add('a');
        set.add('e');
        set.add('i');
        set.add('o');
        set.add('u');        
        
        System.out.println("Set contiene la letra a : " + set.contains('a'));
        //Lo borramos
        set.remove('a');
        System.out.println("Set contiene la letra a : " + set.contains('a'));
        
        //Cremos un Set con el contenido de una String
        Set<Character> s2 = new java.util.HashSet<>();
        String text = "abcdef";
        for(char c : text.toCharArray()){
            s2.add(c);
        }
        
        //Comprobamos si usamos todas las letras existentes en s2 en la variable textoComprobar
        String textoComprobar = "Yo suelo usar el abecedario facil".toLowerCase();
        Set<Character> auxiliar = new java.util.HashSet<Character>(s2);
        for(int i = 0; i < textoComprobar.length(); i++){
            auxiliar.remove(textoComprobar.charAt(i));
        }
        System.out.println("Contiene todas las letras método 1: " + auxiliar.isEmpty());
        
        //Otra opción
        Set<Character> aComprobar = new java.util.HashSet<>();
        for(char c : textoComprobar.toCharArray()){
            aComprobar.add(c);
        }
        System.out.println("Se usan todas las letras: " + aComprobar.containsAll(s2));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listdemo;

import java.util.Map.Entry;
import listdemo.entidades.Persona;

/**
 *
 * @author rafa
 */
public class MapDemo {
    
    public static void main(String[] args){
        ejemploNotas();
        
        //Cuenta las palabras existentes en una String
        java.util.Map<String, Integer> resultado = contadorPalabras("Este es un texto de prueba y es un elemento importante de testeo".toLowerCase());
        System.out.println(resultado);
        
        //Categori
    }
    
    private static void ejemploNotas(){
        java.util.Map<String, Double> notas = new java.util.HashMap<>();
        notas.put("Pepito", 7.5);
        notas.put("Juan", 8.2);
        notas.put("Ricardo", 9d);
        notas.put("Angela", 10d);
        notas.put("Elisa", 7.5);
        System.out.println(notas);
        
        //Muestro la nota de Pepito
        System.out.println("Nota de pepito: " + notas.get("Pepito"));
        
        //Si no existe la clave devuelve null el método get
        System.out.println("Nota de Mariano: " + notas.get("Mariano"));
        //Recorrer un HashMap
        for(String clave : notas.keySet()){
            System.out.println("[" + clave + "]: " + notas.get(clave));
        }
        
        //Creamos las personas
        java.util.Collection<Persona> personas = new java.util.ArrayList<>();
        personas.add(new Persona("1111111", "Pepito", 18, 7));
        personas.add(new Persona("2222222", "María", 19, 4));
        personas.add(new Persona("3333333", "Juan", 19, 3));
        personas.add(new Persona("4444444", "Marta", 18, 8));
        personas.add(new Persona("5555555", "Evaristo", 20, 9));
        java.util.Map<Integer, java.util.List<Persona>> resultado = categorizarPorEdad(personas);
        System.out.println("Por edad: " + resultado);
    }
    
    private static java.util.Map<String, Integer> contadorPalabras(String texto){
        java.util.Map<String, Integer> contadorPalabras = new java.util.HashMap<>();
        String[] palabras = texto.split(" ");
        for(String palabra : palabras){
            if(contadorPalabras.containsKey(palabra)){
                contadorPalabras.put(palabra, contadorPalabras.get(palabra) + 1 );
            }
            else{
                contadorPalabras.put(palabra, 1 );
            }
        }
        return contadorPalabras;
    }
    
    private static java.util.Map<Integer, java.util.List<Persona>> categorizarPorEdad(java.util.Collection<Persona> p){
        java.util.Map<Integer, java.util.List<Persona>> porEdad = new java.util.HashMap<>();
        java.util.Iterator<Persona> it = p.iterator();
        while (it.hasNext()) {
            Persona next = it.next();
            if(porEdad.containsKey(next.getEdad())){
                porEdad.get(next.getEdad()).add(next);
            }
            else{
                java.util.List<Persona> auxiliar = new java.util.ArrayList<>();
                auxiliar.add(next);
                porEdad.put(next.getEdad(), auxiliar);
            }
        }
        return porEdad;
    }
}
